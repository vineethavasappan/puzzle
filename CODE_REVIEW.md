Code Smells or Improvement:

1. Removed unneccessary code : async pipe is used for displaying data
2. Removed the formatDate function : instead added a pipe 
3. On loading the page,it will be better to add some books details instead of 'try searching for a 
    topic, for example "javascript"'

Web-Accessibility Issues on Automated scan:

1. Buttons do not have accessible name. 
2. Background and foreground colors do not have a sufficient contrast ratio.

Web-Accessibility Issues on manual checking: 

1. Image tag don't have alt : added alt in img tag
2. Button do not have label : added aria-label to want to read button
3. User is unable to understand 'want to read' button is selected or not because of it's background    
   color : changed the color